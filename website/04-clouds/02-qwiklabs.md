---
layout: page
title: Облака для изучения Google Clouds и AWS
permalink: /clouds/qwiklabs/
---

# Облака для изучения Google Clouds и AWS

<br/>

Если есть желание изучать облака Google или Amazon, есть серви который подготовил для этого лабораторки и облачные инстансы. Можно пользоваться. Сам сервис хочет зарабатывать, но время от времени подкибывает халявы. Кому интересно, может попробовать. 

Иногда халяву можно найти на сайте: 

https://medium.com/@sathishvj/qwiklabs-free-codes-gcp-and-aws-e40f3855ffdb

<br/>

1) Зарегайся на qwiklabs.com

2) В строке адреса вбей.

https://drftclk-631.com/click/575cd0f5-b716-43b9-8eba-b56a7d82117b?u=https://go.qwiklabs.com/30-day-challenge-1019#utm_source=drift&utm_medium=email&utm_campaign=oct30&h=dad684cb5fd052c24cc859a902db7b3d

3) Введи код: 1q-devthirty-55

4) И получи халяву.


<br/>

**Подробнее:**

<br/>

Hi Marley,

October might be full of ghosts, witches, and evil clowns, but if there’s one thing Advantage members don’t have to be scared of, it’s missing out on all the great quests Qwiklabs has to offer this month!

First things first: it’s time for another 30 Day Challenge! Cloud Development gives app developers hands-on practice with state-of-the-art tools – and it’s great prep for taking the Google Cloud Certified Professional Cloud Developer Certification exam! 

Use code 1q-devthirty-55 to enroll. Complete the Cloud Dev quest by October 31st to win prizes, glory, and more (it will mostly be prizes and glory, though.)

https://drftclk-631.com/click/575cd0f5-b716-43b9-8eba-b56a7d82117b?u=https://go.qwiklabs.com/30-day-challenge-1019#utm_source=drift&utm_medium=email&utm_campaign=oct30&h=dad684cb5fd052c24cc859a902db7b3d


That’s not the only treat we have for you this Halloween. Add these two new quests to your bag!

Understanding GCP Costs – Learn to set up a billing accounts, organize resources, and manage billing access permissions. 

DevOps Essentials – DevOps is in the Cloud! Improve the speed, stability, availability, and security of your software delivery.

Finally! In September, you completed 201 minutes of learning – way to go!

See you in the quests – and in the challenge! 

Halle at Qwiklabs

PS: Check out our weekly speedrun game, and play along with Qwiklabs pros during our YouTube livestreams!