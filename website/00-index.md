---
layout: page
title: Материалы для изучения JavaScript и JavaScript фреймворков на русском языке
permalink: /
---

# Материалы для изучения JavaScript и JavaScript фреймворков на русском языке

<br/>

### Домен перешел в руки не самого способного, но достаточно общительного разработчика

<br/>

### Основной сайт <a href="https://jsdev.org">jsdev.org</a>

<br/>

### Предлагаю на этих выходных развернуть js приложение (react/nodejs) в локальном Kubernetes кластере

Само приложение можно взять <a href="https://github.com/marley-nodejs/MERN-Stack-Front-To-Back-v2.0">здесь</a>

А kubernetes кластер поднимается скриптами и для его запуска, достаточно всего выполнить следующие <a href="https://sysadm.ru//linux/servers/containers/kubernetes/kubeadm/prepared-cluster/">инструкции</a>. Правда все это подготовлено для linux.


Далее, я бы хотел получить ценную информацию, как сделать Server Side Rendering для этого приложения. Но это уже следующий этап.

<br/>

Тем, кто хочет, но пока не знает, что это такое и с чего начать, рекомендую скачать/купить следующие материалы и <a href="/courses/eng/">ознакомиться с ними</a>


<br/>

### Совместное изучение js

В настоящее время, админ сайта изучает видеокурс. 

**[Udemy, Stephen Grider] Typescript: The Complete Developer's Guide [2019, ENG]**

Думаю, что его должны посмотреть все кто программирует на node.js, но так и не освоил TypeScript. Для node.js разработчика, появляются достаточно мощные дополнительные инструмены, позволяющие писать более надеждный код. Кто хочет, подключайтесь, догоняйте. Врочем, для тех кто незнаком с ООП, материал будет крайне сложным.

Потом, наверное нужно будет посмотреть курсы: 

* [Udemy] Complete Guide to Elasticsearch
* [Udemy, Andrew Mead] The Modern GraphQL Bootcamp (Advanced Node.js) [2018, ENG]

<br/>

Новый курс от Brad Traversy. Можно купить по промо кодам. (Это не рефки. Я с этого ничего не буду иметь) 

* [Udemy, Brad Traversy] Node.js API Masterclass With Express & MongoDB [2019, ENG]

Special 3 day coupon: $9.99 (Ends on the 19th)  
https://www.udemy.com/course/nodejs-api-masterclass/?couponCode=OCTOBER999

30 day $13.99 coupon  
https://www.udemy.com/course/nodejs-api-masterclass/?couponCode=OCTOBER1399

<br/>

Можете предложить что-то еще, что следует поделать после завершения изучения материала по TypeScript.

<br/>
<br/>

**Обсуждаем всякую ерунду, обменваемся материалами, обсуждаем вакансии <a href="/chat/">в телеграм чате</a>**

**Скидываемся на покупку премиум аккаунтов для скачивания курсов с coursehunters, pluralsight, rapidgator, etc <a href="/coursehunters-skladchina/">здесь</a>**

