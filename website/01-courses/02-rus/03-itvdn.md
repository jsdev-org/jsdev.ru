---
layout: page
title: Материалы с сайта ITVDN
permalink: /courses/itvdn/
---


# Материалы с сайта ITVDN


<br/>

### [ITVDN] React.JS Advanced [2017, RUS]

<br/>

### [ITVDN] ECMAScript 6 [2017, RUS]

<br/>

### ITVDN | Angular2 Essential (2017) PCRec


<br/>

### [ITVDN] Видео курс JavaScript Шаблоны / Video course JavaScript Patterns [2015, RUS]


<br/>

### ITVDN | Node.js (2017) PCRec [H.264]


<br/>

### Круто! Прямые ссылки на скачивание материалов от правообладателя!

Из Google прислали какое-то письмо о DMCA.  
https://www.lumendatabase.org/notices/14868545


Если перейти на нее и кликнуть на <strong>ООО "КиберБионик Систематикс"</strong> или на <strong>Охрименко Дмитрий Валерьевич</strong>, то можно получить информацию о том, какие материалы были удалены из поиска и прямые ссылки на их скачивание.

Вот это, я понимаю работа!
Я даже не знал о таких трекерах.
