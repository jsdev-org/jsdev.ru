---
layout: page
title: Видеокурсы для изучения JavaScript и JavaScript фреймворков на англияком языке
permalink: /courses/eng/
---

# Видеокурсы для изучения JavaScript и JavaScript фреймворков на англияком языке

Наиболее интересные материалы для максимального погружения в разработку с использованием js. Да материалы на английском. Материал на английском, обычно можно подобрать более качественный, в силу того, что просто больше выбор.

Разумеется все субъективно.

* [Udemy, Andrew Mead] The Complete Node.js Developer Course (3rd Edition) [2019, ENG]
* [Udemy, Stephen Grider] Typescript: The Complete Developer's Guide [2019, ENG] (Тоже нужно для серверного javascript)
* [Udemy, Brad Traversy] MERN Stack Front To Back: Full Stack React, Redux & Node.js [2019, ENG]
* [Udemy, Stephen Grider] Docker and Kubernetes: The Complete Guide [2018, ENG]

<br/>

Наверное, потом нужно будет пересмотреть: 

* [Udemy, Andrew Mead] The Modern GraphQL Bootcamp (Advanced Node.js) [2018, ENG]
* [Udemy, Stephen Grider] Advanced React and Redux: 2018 Edition [Udemy, ENG, 2018]
* [Udemy, Stephen Grider] Server Side Rendering with React and Redux [Udemy, ENG, 2017]
